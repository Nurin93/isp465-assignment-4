/*Name: NURIN SYAZWANI BINTI MOHD KHAIRI
  Matric ID: 2020483176
  Assignment: Lab Assignment 4 (JavaScript)*/

function CorrectAns(){
	var no1 = document.forms["myForm"]["no1"].value;
	var no2 = document.forms["myForm"]["no2"].value;
	var no3 = document.forms["myForm"]["no3"].value;
	var no4 = document.forms["myForm"]["no4"].value;
	var no5 = document.forms["myForm"]["no5"].value;
	var no6 = document.forms["myForm"]["no6"].value;
	var no7 = document.forms["myForm"]["no7"].value;
	var no8 = document.forms["myForm"]["no8"].value;
	var no9 = document.forms["myForm"]["no9"].value;
	var no10 = document.forms["myForm"]["no10"].value;
	var name = document.forms["myForm"]["name"].value;
  
  //store the radio button answer in an array
  var Answers;
  Answers = [no1, no2, no3, no4, no5, no6, no7, no8, no9, no10];
  
  //call the array to make sure user answer all question
  for(var i=0; i<10; i++){
  	if (Answers[i] == ""){
  		alert("Oops!! Question "+ (i+1) +" is required");
			return false;
  	}
 	}
  
  //to make sure user input his name before submit the answers
	if (name == ""){
		alert("Oops!! Please enter your name!");
		return false;
	}

	//check answer whether it is correct or not 
	var ans = 0;

	if (no1 === "Cascading"){	
		ans++;
	}
	if (no2 === "background-color"){	
		ans++;
	}
	if (no3 === "line-height"){	
		ans++;
	}
	if (no4 === "blank"){	
		ans++;
	}
	if (no5 === "b"){	
		ans++;
	}
	if (no6 === "h1"){	
		ans++;
	}
	if (no7 === "color"){	
		ans++;
	}
	if (no8 === "a:focus"){	
		ans++;
	}
	if (no9 === "padding"){	
		ans++;
	}
	if (no10 === "br"){	
		ans++;
	}
  
	//display how many correct answer that user get
	if(ans >= 0 && ans <= 4 ){
	alert("Keep trying, " + name + "! You answered " + ans + " out of 10 correctly");
	}
	
	else if (ans >= 5 && ans<=9){
		alert("Way to go, " + name + "! You got " + ans +" out of 10 correct ");
	}

	else{
		alert("Congratulations " + name + "! You got " + ans + " out of 10");
	}
}
